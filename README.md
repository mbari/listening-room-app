# Soundscape Listening Room

This application allows users to browse a collection of audio clips that are hosted on Freesound.org.  Buttons are used to filter the clips and then users can play the sounds using embedded players from Freesound.org.  At MBARI, the deployment of this application was a little complicated because I could not find a way to directly check out the project into the wwwroot share that was mounted on a machine that had git installed.  There was something with permissions that would not let me clone this project directly onto the wwwroot share so the process takes a couple more steps.

## To add a new sound clip

1. Add the clip to Freesound under the MBARI_MARS user account.
1. Make sure you tag the sound clip in Freesound with a tag that you will be using to organize the sound clips by.  For example, if I uploaded a sound clip of an orca, I should tag the clip with 'orca'.
1. Clone this repository to a local directory on your machine.
1. Make sure that Pyhton 2.7 is installed on the machine where you checked out the code.
1. From the command line, change directories so that you are in the 'python' subdirectory and run:
```bash
FREESOUND_API_KEY='xxxxxxxxxxxxx' python get-api-data.py
```
1. This python script will use the FREESOUND_API_KEY to make a call to the Freesound API and will query for the full list of clips (and their metadata) from the website.  It will write all of this information to a file named results.json in the data/api folder on your local install.
1. Mount the production share (wwwroot) on your machine somewhere.
1. Make a backup copy of the wwwroot/at-sea/cabled-observatory/mars-science-experiments/mars-hydrophone/listening-room-app/data/api/results.json file.
1. Copy the data/api/results.json file from your local machine to the wwwroot/at-sea/cabled-observatory/mars-science-experiments/mars-hydrophone/listening-room-app/data/api directory, overwriting the one that is there.  This essentially deploys the latest 'catalog' of soundclips that are available under the MBARI_MARS account.
1. If you need to create a new 'category' (i.e. filter) for the application, you will need an icon image and you will need to edit the config.json file to add the new category.  For example, if I added a new clip that I tagged with 'orca', but I did not have an Orca category yet, I would need to get a copy of the image I wanted to use on the button and copy it to the wwwroot/at-sea/cabled-observatory/mars-science-experiments/mars-hydrophone/listening-room-app/images directory.  Then I would need to edit the wwwroot/at-sea/cabled-observatory/mars-science-experiments/mars-hydrophone/listening-room-app/config.json file and add a new section that might look something like:
```json
    {
      "id": 2,
      "name": "Killer Whale (Orca)",
      "description": "Wolves of the sea",
      "tag": "orca",
      "image": "images/orca.png"
    },
```
1. Note that the 'id' field on these entries is arbitrary, but they just need to be unique in each category, what their values are does not matter.

## Notes

1. The Freesound API limits the number of calls that can be made so to ensure we don't hit that limit (and for performance reasons) we only make a call to the Freesound API when the content on Freesound changes.  There is a Python script in the python sub directory that can be run to generate the file that is stored locally and then read by the web application.  You will need to do this from a terminal/command line window and in that shell you will need to have and environment variable set that contains the API key used to call the Freesound API.  You can either set the environment variable in the terminal/command line session or specify it on the command line when you run the python command. You need to have Python 2.7 installed, then from a terminal/command line window, change into the python subdirectory and run:
``` bash
FREESOUND_API_KEY='xxxxxxxxxxxxx' python get-api-data.py
```
1. The above command will generate a 'results.json' file in the data/api subdirectory which is then read by the web application.  If in development, you can run this command and then just refresh the web application and it will pick up any changes from Freesound.
1. Normally, in production, you would run this command locally and then copy the results.json file to the data/api directory on the production web server.
1. If you want to add a new category, you need to put an image file in the 'images' directory that will be used on the new filter button on the web page.  You will then want to edit the config.json file to add a new entry giving it a sequentially increasing ID (bump the last ID by one), specifying what image you are using and, most importantly, specifying the correct tag that will be used in to match the new category to sound clips in Freesound that are tagged with that name.

## Developer Notes

For development, you can checkout this project, make a copy of the config.json.template file and call it config.json and edit it to fit your needs.  Then run:

``` bash
# Run npm install to install all dependencies in the node_modules directory
npm install

# As you are developing, you can see live updates in your browser by running
# the following which will display the application in your browser 
# at http://localhost:8080
npm run dev

# When you are ready to deploy the application, run
npm run build
```
The last steps builds a minified, combined java script library in the 'dist' directory. Then you can copy the following to a directory that is served by an HTTP server and your app should be deployed:

1. config.json (edit this to add/remove buttons)
1. images (contains all the images referenced in the config.json)
1. dist
1. index.html
1. package.json

You can either copy the node_modules directory over, or you can run the 'npm install' from the command line in the deployment directory to build it automatically.  At MBARI, it's WAY faster to run the npm install locally on your dev machine and then copy the node_modules directory onto the share.

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
