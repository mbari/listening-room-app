import urllib2
import os
import sys
import json

# Grab the API Key from the environment
api_key = os.getenv('FREESOUND_API_KEY', None)
if api_key is None:
    print("You need to set your API key as an evironment variable",)
    print("named FREESOUND_API_KEY")
    sys.exit(-1)

# Create the request to grab the full list of clips for the MBARI_MARS user
req = urllib2.Request('https://freesound.org/apiv2/search/text/?filter=username:MBARI_MARS&token=' + api_key)

# Make the call
response = urllib2.urlopen(req)
the_page = response.read()

# Load the response into JSON to work with it
json_response = json.loads(the_page)

for result in json_response['results']:
    # Create the call to get the details
    details_req = urllib2.Request('https://freesound.org/apiv2/sounds/' + str(result['id']) + '/?token=' + api_key)
    details_response = urllib2.urlopen(details_req)
    details_raw = details_response.read()
    json_details = json.loads(details_raw)
    result['details'] = json_details

result_file = open('../data/api/results.json', 'w')
result_file.write(json.dumps(json_response['results'], indent=2, sort_keys=True))
result_file.close()

