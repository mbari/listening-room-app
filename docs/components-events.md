# Components and Events

Just to keep track of everything that is going on with the components in this 
application, here is a list of the various DOM components and what happens when
the users interact with them.  This is for one sound clip that is connected to three
players (large, medium, small)

## DOM Components
1. div (class=clip-player-container) - wrapper around all three players
    1. h4 - Title of the sound clip
        1. a - link that users can click on to open the clip in a new tab in Freesound
    1. div (class=large large-clip-player) - wrapper around entire large player widget
        1. div (class=widget large) - the large widget div
            1. div (class=sample-info) - wrapper around title bar information
                1. nobr - to prevent line break, should this be done with CSS? This tag is deprecated
                    1. img (class=avatar)- MBARI icon in the title bar
                    1. h3 - clip name in player title bar
                    1. img (class=cc_license) - this is the icon in the far right corner of the title bar showing it's CC license
                    1. a - this is a link to open the sound in freesound (the image is placed with background-image styling)
                1. br (style='clear: both;') - this inserts a break to push the player down inside the widget
            1. div (class=player large) - this is the wrapper around the components that make up the player        
                1. div (class=metadata) - this was a hidden div to hold data about the clip and is replaced by data in 
                the Vue component
                1. div (class=controls) - this is the wrapper around the control buttons
                    1. a (class=toggle play) - this is the link that is the play/pause button
                    1. a (class=button stop) - this is the stop button
                    1. a (class=display toggle) - this is the button to switch between waveform and spectrum views
                    1. a (class=toggle loop) - this is the button to turn on/off looping
                    1. a (class=toggle measure) - this is the button to turn on measuring where mouse is
                1. div (class=background) - this is the div with the background image 
                1. div (class=measure-readout-container) - just that, a wrapper around the db measure readout
                    1. div (class=measure-readout) - the div where the db value is displayed
                1. div (class=loading-progress) - an overlay used to show how much of the clip has been downloaded
                1. div (class=position-indicator) - a div to display the playback progress
                1. div (class=time-indicator-container) - a div to wrap the time progress display
                    1. div (class=time-indicator) - div where the time is written
    1. div (class=medium medium-clip-player) - the wrapper around the entire medium player widget
        1. div (class=widget medium) - the medium widget div
            1. div (class=player_wrapper) - not sure why this one is necessary (for styling?)
                1. div (class=player small) - this is aweful, but they got into some state where the medium player is
                called 'small', but this is the actual player div
                    1. div (class=metadata) - this is a hidden div where data was held, but is replaced by the Vue 
                    components
                    1. div (class=controls) - wrapper div around the control buttons
                        1. a (class=toggle play) - this is the play/pause button
                        1. a (class=toggle loop) - this is the button to turn on/off looping
                    1. div (class=background) - this is the background image
                    1. div (class=loading-progress) - an overlay used to show how much of the clip has been downloaded
                    1. div (class=position-indicator) - a div to display the playback progress
                    1. div (class=time-indicator-container) - a div to wrap the time progress display
                        1. div (class=time-indicator) - div where the time is written
            1. div (class=sample_info) - div container for the title bar and tags
                1. span (class=title) - wrapper around the title in the title bar
                    1. nobr - to prevent line break, should this be done with CSS? This tag is deprecated
                    1. h3 - the title in the title bar
                    1. img (class=cc_license) - this is the CC license image to the right in the title bar
                1. div (class=sample_info_left) - the wrapper around the MBARI logo, user name and freesound link
                    1. img (class=avatar) - this is the MBARI logo
                    1. span (class=username) - this is the MBARI username
                    1. a - link to open clip in freesound
                1. div (class=sample_info_right) - wrapper around tag listing
                    1. ul (id=tags) - the tag listing
                        1. li - one for each tag
                            1. span - where the tag name goes
                1. br (style=clear both)
    1. div (class=small small-clip-player) - the wrapper around the entire small player widget
        1. div (class=widget small) - wrapper around the small widget
            1. nobr - to prevent line break, should this be done with CSS? This tag is deprecated
                1. div (class=player mini) - yes, small player is now called 'mini' ... grrr
                    1. div (class=metadata) - this is a hidden div to hold data and is replaced by Vue component
                    1. div (class=controls) - wrapper around controls
                        1. a (class=toggle play) - play/pause button
                        1. a (class=loop) - button to turn on/off looping
                    1. div (class=background) - this is the background image
                    1. div (class=loading-progress) - an overlay used to show how much of the clip has been downloaded
                    1. div (class=position-indicator) - a div to display the playback progress
    
## Non DOM Objects

1. SoundManager
2. soundClip - the actual sound that is played and is linked to all three players

## Events

1. while loading javascript
    1. elements with class loading-progress -> display: none;
1. user clicks play/pause
1. user clicks stop
1. user clicks loop
1. user clicks measure
1. user hovers over background image
1. user clicks on time readout

